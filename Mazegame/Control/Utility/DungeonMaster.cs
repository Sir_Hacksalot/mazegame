
using System;
using Mazegame.Boundary;
using Mazegame.Entity;
using System.Collections;

namespace Mazegame.Control
{
    public class DungeonMaster
    {
        private IMazeClient gameClient;
        private IMazeData gameData;
        private Player thePlayer;
        private CommandHandler playerTurnHandler;

        public DungeonMaster()
        {
            this.gameData = GameFactory.GetInstance().TheData;
            this.gameClient = GameFactory.GetInstance().TheClient;
            playerTurnHandler = new CommandHandler();
        }

        public void PrintWelcome()
        {
            gameClient.PlayerMessage(gameData.GetWelcomeMessage());
        }

        public void SetupPlayer()
        {
            String playerName = gameClient.GetReply("What is your name, brave hero?");
            thePlayer = new Player(playerName);
            thePlayer.Inventory.Gold.Add(150);
            thePlayer.CurrentLocation = gameData.GetStartingLocation();
            gameClient.PlayerMessage("Welcome " + playerName + "\n\n");
            gameClient.PlayerMessage("You are about to embark on a tedious quest. \nI pray your death will be swift.\n\n");
            gameClient.PlayerMessage("Where are you? Try having a LOOK around.");
        }

        public void RunGame()
        {
            PrintWelcome();
            SetupPlayer();

            while (handlePlayerTurn())
            {
                //handle npc logic later here
            }

            gameClient.GetReply("\n\n<<Hit enter to exit>>");
        }

        private bool handlePlayerTurn()
        {
            CommandResponse playerResponse = playerTurnHandler.ProcessTurn(gameClient.GetCommand(), thePlayer);
            gameClient.PlayerMessage(playerResponse.Message);
            return !playerResponse.FinishedGame;
        }
    }
}