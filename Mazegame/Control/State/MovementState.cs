﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class MovementState : CommandState
    {
        public MovementState() : base()
        {
            AvailableCommands.Add("go", new MoveCommand());
            AvailableCommands.Add("quit", new QuitCommand());
            AvailableCommands.Add("move", new MoveCommand());
            AvailableCommands.Add("look", new LookCommand());
            AvailableCommands.Add("list", new ListCommand());
            AvailableCommands.Add("unlock", new UnlockCommand());
            AvailableCommands.Add("take", new TakeCommand());
            AvailableCommands.Add("get", new TakeCommand());
            AvailableCommands.Add("drop", new DropCommand());
            AvailableCommands.Add("unequip", new UnEquipCommand());
            AvailableCommands.Add("equip", new EquipCommand());
            AvailableCommands.Add("talk", new SpeakCommand());
            AvailableCommands.Add("speak", new SpeakCommand());
            AvailableCommands.Add("say", new SpeakCommand());
            AvailableCommands.Add("fight", new AttackCommand());
            AvailableCommands.Add("kill", new AttackCommand());
            AvailableCommands.Add("attack", new AttackCommand());
        }
        public override CommandState Update(Player thePlayer)
        {
            if (thePlayer.CurrentLocation is Shop)
            {
                return new ShopState();
            }
            else if(thePlayer.IsFighting)
            {
                return new CombatState();
            }
            else if (thePlayer.CurrentLocation is Brothel)
            {
                return new BrothelState();
            }
            else if (thePlayer.CurrentLocation is Hotel)
            {
                return new HotelState();
            }
            else if (thePlayer.CurrentLocation is Church)
            {
                return new ChurchState();
            }
            else
            {
                return this;
            }
        }
    }
}
