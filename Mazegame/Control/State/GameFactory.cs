﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Mazegame.Boundary;

namespace Mazegame.Control
{
    public class GameFactory
    {
        Dictionary<string, string> config = new Dictionary<string, string>();
        private IMazeClient theClient;
        private IMazeData theData;
        private static GameFactory instance;

        public GameFactory()
        {
            ReadConfigFile();
            theClient = (IMazeClient)DynamicLoad(config["ui_assembly"], config["ui"]);
            theData = (IMazeData)DynamicLoad(config["data_assembly"], config["data"]);
        }

        public IMazeClient TheClient
        {
            get { return theClient; }
            set { theClient = value; }
        }

        public IMazeData TheData
        {
            get { return theData; }
            set { theData = value; }
        }

        private void ReadConfigFile()
        {
            try
            {
                using (StreamReader reader = new StreamReader("config.txt"))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] items = reader.ReadLine().Split('=');
                        config.Add(items[0], items[1]);
                    }
                }
            }
            catch (Exception)
            {

                Console.WriteLine("Error reading from config file");
                Environment.Exit(0);
            }        
        }

        private Object DynamicLoad(string assemblyName, string className)
        {
            Assembly assembly = Assembly.LoadFrom(assemblyName);

            //walk through each type in the assembly
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsClass)
                {
                    if(type.FullName.EndsWith("." + className))
                    {
                        return Activator.CreateInstance(type);
                    }
                }
            }
            throw (new Exception("could not instantiate class"));
        }

        public static GameFactory GetInstance()
        {
            if (instance == null)
            {
                instance = new GameFactory();
            }

            return instance;
        }
    }
}
