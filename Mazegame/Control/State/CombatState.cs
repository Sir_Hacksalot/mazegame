﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class CombatState : CommandState
    {
        public CombatState() : base()
        {        
            AvailableCommands.Add("quit", new QuitCommand());
            AvailableCommands.Add("fight", new AttackCommand());
            AvailableCommands.Add("kill", new AttackCommand());
            AvailableCommands.Add("attack", new AttackCommand());
            AvailableCommands.Add("flee", new FleeCommand());
            AvailableCommands.Add("run", new FleeCommand());
        }

        public override CommandState Update(Player thePlayer)
        {
            if (thePlayer.CurrentLocation is Shop)
            {
                return new ShopState();
            }
            else if (thePlayer.CurrentLocation is Brothel)
            {
                return new BrothelState();
            }
            else if (thePlayer.CurrentLocation is Hotel)
            {
                return new HotelState();
            }
            else if(thePlayer.IsFighting)
            {
                return this;
            }
            else if (thePlayer.CurrentLocation is Church)
            {
                return new ChurchState();
            }
            else
            {
                return new MovementState();
            }
        }
    }
}
