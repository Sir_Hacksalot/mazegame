﻿using Mazegame.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Control
{
    public class BrothelState : HotelState
    {
        public BrothelState() : base()
        {
            AvailableCommands.Add("fuck", new RootCommand());
            AvailableCommands.Add("bang", new RootCommand());
            AvailableCommands.Add("root", new RootCommand());
            AvailableCommands.Add("screw", new RootCommand());
            AvailableCommands.Add("tap", new RootCommand());
            AvailableCommands.Add("sex", new RootCommand());
            AvailableCommands.Add("bonk", new RootCommand());
            AvailableCommands.Add("lick", new RootCommand());
            AvailableCommands.Add("suck", new RootCommand());
            AvailableCommands.Add("blow", new RootCommand());
            AvailableCommands.Add("wank", new RootCommand());
            AvailableCommands.Add("sleep", new RootCommand());
        }

        public override CommandState Update(Player thePlayer)
        {
            if (thePlayer.CurrentLocation is Shop)
            {
                return new ShopState();
            }
            else if (thePlayer.IsFighting)
            {
                return new CombatState();
            }
            else if (thePlayer.CurrentLocation is Brothel)
            {
                return this;
            }
            else if (thePlayer.CurrentLocation is Hotel)
            {
                return new HotelState();
            }
            else if (thePlayer.CurrentLocation is Church)
            {
                return new ChurchState();
            }
            else
            {
                return new MovementState();
            }
        }
    }
}
