﻿using Mazegame.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Control
{
    class ChurchState : MovementState
    {
        public ChurchState() : base()
        {
            AvailableCommands.Add("pray", new PrayCommand());
            AvailableCommands.Add("worship", new PrayCommand());
            AvailableCommands.Add("prayer", new PrayCommand());
        }

        public override CommandState Update(Player thePlayer)
        {
            if (thePlayer.CurrentLocation is Shop)
            {
                return new ShopState();
            }
            else if (thePlayer.IsFighting)
            {
                return new CombatState();
            }
            else if (thePlayer.CurrentLocation is Brothel)
            {
                return new BrothelState();
            }
            else if (thePlayer.CurrentLocation is Hotel)
            {
                return new HotelState();
            }
            else if (thePlayer.CurrentLocation is Church)
            {
                return this;
            }
            else
            {
                return new MovementState();
            }
        }
    }
}
