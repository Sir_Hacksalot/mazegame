﻿using Mazegame.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Control
{
    public class ShopState : CommandState
    {
        public ShopState() : base()
        {
            AvailableCommands.Add("go", new MoveCommand());
            AvailableCommands.Add("quit", new QuitCommand());
            AvailableCommands.Add("move", new MoveCommand());
            AvailableCommands.Add("look", new LookCommand());
            AvailableCommands.Add("list", new ListCommand());
            AvailableCommands.Add("buy", new BuyCommand());
            AvailableCommands.Add("sell", new SellCommand());
            AvailableCommands.Add("talk", new SpeakCommand());
            AvailableCommands.Add("speak", new SpeakCommand());
            AvailableCommands.Add("say", new SpeakCommand());
            AvailableCommands.Add("unequip", new DropCommand());
            AvailableCommands.Add("equip", new EquipCommand());

        }
        public override CommandState Update(Player thePlayer)
        {
            if (thePlayer.CurrentLocation is Shop)
            {
                return this;
            }
            else if (thePlayer.IsFighting)
            {
                return new CombatState();
            }
            else if (thePlayer.CurrentLocation is Brothel)
            {
                return new BrothelState();
            }
            else if (thePlayer.CurrentLocation is Hotel)
            {
                return new HotelState();
            }
            else
            {
                return new MovementState();
            }
        }
    }
}
