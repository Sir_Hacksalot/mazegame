﻿using Mazegame.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Control
{
    public class HotelState : MovementState
    {
        public HotelState() : base()
        {
            AvailableCommands.Add("drink", new DrinkCommand());
            AvailableCommands.Add("beer", new DrinkCommand());
            AvailableCommands.Add("ale", new DrinkCommand());
        }

        public override CommandState Update(Player thePlayer)
        {
            if (thePlayer.CurrentLocation is Shop)
            {
                return new ShopState();
            }
            else if (thePlayer.IsFighting)
            {
                return new CombatState();
            }
            else if (thePlayer.CurrentLocation is Brothel)
            {
                return new BrothelState();
            }
            else if(thePlayer.CurrentLocation is Hotel)
            {
                return this;
            }
            else if (thePlayer.CurrentLocation is Church)
            {
                return new ChurchState();
            }
            else
            {
                return new MovementState();
            }
        }
    }
}
