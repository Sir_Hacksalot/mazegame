﻿using Mazegame.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Control
{
    public abstract class CommandState
    {
        private Hashtable availableCommands;

        public CommandState()
        {
            availableCommands = new Hashtable();
        }

        protected Hashtable AvailableCommands
        {
            get { return availableCommands; }
            set { availableCommands = value; }
        }

        public abstract CommandState Update(Player thePlayer);

        public Command getCommand(string commandLabel)
        {
            try
            {
                return (Command)availableCommands[commandLabel];
            }
            catch (KeyNotFoundException)
            {

                return null;
            }
        }

        public ArrayList getLabels()
        {
            return new ArrayList(availableCommands.Keys);
        }
    }
}
