﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    class DrinkCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            Hotel hotel = (Hotel)thePlayer.CurrentLocation;
            return new CommandResponse(hotel.getDrink(thePlayer));
        }
    }
}
