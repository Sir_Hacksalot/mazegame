﻿using Mazegame.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Control
{
    class DropCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            CommandResponse response = new Control.CommandResponse("You don't have that object.");

            if (userInput.Arguments.Count == 0)
            {
                response.Message = thePlayer.CurrentLocation.ToString();
                return response;
            }
            else
            {
                foreach (string argument in userInput.Arguments)
                {
                    foreach (DictionaryEntry entry in thePlayer.Inventory.Items)
                    {
                        if (entry.Key.Equals(argument))
                        {
                            thePlayer.dropItem(argument, (Item)entry.Value);
                            response.Message = "You drop the " + argument;
                            return response;
                        }
                    }
                }
                return response;
            }

        }
    }
}
