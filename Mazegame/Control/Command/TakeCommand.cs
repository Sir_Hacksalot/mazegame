﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;
using System.Collections;

namespace Mazegame.Control
{
    public class TakeCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            CommandResponse response = new CommandResponse("You can't have that object.");

            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("I don't know what you want.\n" + thePlayer.CurrentLocation.ToString());
            }
            else
            {
                foreach (string argument in userInput.Arguments)
                {
                    foreach (DictionaryEntry entry in thePlayer.CurrentLocation.Inventory.Items)
                    {
                        if(entry.Key.Equals(argument))
                        {   
                            response.Message = thePlayer.getItem(argument, (Item)entry.Value);
                            return response;
                        }
                    }
                }
                return response;
            }

        }
    }
}
