﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;
using System.Collections;

namespace Mazegame.Control
{
    public class UnEquipCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            CommandResponse response = new CommandResponse("You don't have that object.");

            if (userInput.Arguments.Count == 0)
            {
                response.Message = thePlayer.CurrentLocation.ToString();
                return response;
            }
            else
            {
                foreach (string argument in userInput.Arguments)
                {
                    foreach (DictionaryEntry entry in thePlayer.Inventory.Items)
                    {
                        if (entry.Key.Equals(argument))
                        {
                            if(thePlayer.unequipItem((Item)entry.Value))
                            {
                                return new CommandResponse("You unequip the " + argument);
                            }
                            else
                            {
                                return new CommandResponse("You can't unequip the " + argument);
                            }                
                        }
                    }
                }
                return response;
            }
        }
    }
}
