﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;
using System.Collections;

namespace Mazegame.Control
{
    public class BuyCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            CommandResponse response = new CommandResponse("That isn't going to work.");

            //check for an argument
            if (userInput.Arguments.Count == 0)
            {
                response.Message = thePlayer.CurrentLocation.ToString();
                return response;
            }
            else
            {
                foreach (string argument in userInput.Arguments)
                {
                    foreach (DictionaryEntry entry in thePlayer.CurrentLocation.Inventory.Items)
                    {
                        if (entry.Key.Equals(argument.ToLower()))
                        {
                            Item item = (Item)entry.Value;
                            Shop shop = (Blacksmith)thePlayer.CurrentLocation;
                            return new CommandResponse(shop.SellItem(thePlayer, item, argument));                                                  
                        }
                    }
                }
                return response;
            }
        }
    }
}
