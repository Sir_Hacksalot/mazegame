﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class PrayCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            Church church = (Church)thePlayer.CurrentLocation;
            return new CommandResponse(church.pray(thePlayer));
        }
    }
}
