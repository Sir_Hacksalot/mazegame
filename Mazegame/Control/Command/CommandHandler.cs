﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;


namespace Mazegame.Control
{
    public class CommandHandler
    {
        private CommandState availableCommands;

        public CommandHandler()
        {
            availableCommands = new MovementState();
        }
        
        public CommandResponse ProcessTurn(string userInput, Player thePlayer)
        {
            availableCommands = availableCommands.Update(thePlayer);

            ParsedInput validInput;

            if (thePlayer.CurrentLocation is Brothel)
            {
                //no filter
                validInput = noParse(userInput);
            }
            else
            {
                validInput = parse(userInput);
            }
            
            Command theCommand = availableCommands.getCommand(validInput.Command);
            if (theCommand == null)
            {
                return new CommandResponse("Not a valid command");
            }

            return theCommand.Execute(validInput, thePlayer);

        }

        private ParsedInput parse(String userInput)
        {
            Parser theParser = new Parser(availableCommands.getLabels());
            return theParser.Parse(userInput);
        }
        private ParsedInput noParse(String userInput)
        {
            Parser theParser = new Parser(availableCommands.getLabels());
            return theParser.noFilterParse(userInput);
        }
    }
}
