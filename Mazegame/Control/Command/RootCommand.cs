﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    public class RootCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            Brothel brothel = (Brothel)thePlayer.CurrentLocation;
            return new CommandResponse(brothel.OohMatron(act(userInput), thePlayer));
        }

        private string act(ParsedInput userInput)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(userInput.Command);

            foreach (var item in userInput.Arguments)
            {
                sb.Append(" " + item);
            }
            return sb.ToString();
        }
    }
}
