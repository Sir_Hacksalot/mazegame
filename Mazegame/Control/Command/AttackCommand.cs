﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;
using System.Collections;

namespace Mazegame.Control
{
    public class AttackCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("You want to kill who?");
            }

            String npcName = (String)userInput.Arguments[0];

            NonPlayerCharacter desiredNPC = thePlayer.CurrentLocation.GetNPC(npcName);
            return fightPrep(thePlayer, npcName, desiredNPC);

        }

        private static CommandResponse fightPrep(Player thePlayer, string npcName, NonPlayerCharacter desiredNPC)
        {
            if (desiredNPC == null)
            {
                return new CommandResponse("There is nobody by that name");
            }
            else if (!desiredNPC.Hostile)
            {
                return new CommandResponse("You can't fight that person.");
            }
            else
            {
                //found him, now lets have a barney
                //thePlayer.IsFighting = true;

                Combat combat = new Combat(thePlayer, npcName);
                if (combat.fight())
                {
                    thePlayer.IsFighting = false;
                    return new CommandResponse("You have killed your enemy");
                }
                else
                {
                    thePlayer.IsFighting = false;
                    return new CommandResponse("You died.", true);
                }
            }
        }
    }
}
