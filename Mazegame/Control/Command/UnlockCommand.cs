﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;
using System.Collections;

namespace Mazegame.Control
{
    public class UnlockCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            CommandResponse response = new CommandResponse("That isn't going to work.");

            //check for zero input
            if (userInput.Arguments.Count == 0)
            {
                response.Message = thePlayer.CurrentLocation.ToString();
                return response;
            }

            //check that the exit exists
            bool success = false;

            foreach (string i in userInput.Arguments)
            {
                if (thePlayer.CurrentLocation.ContainsExit(i))
                {
                    Exit theExit = thePlayer.CurrentLocation.GetExit(i);
                    if (theExit.IsLocked)
                    {
                        //get item from inventory of type key
                        foreach (DictionaryEntry entry in thePlayer.Inventory.Items)
                        {
                            if(entry.Key.Equals("key"))
                            {
                                //use the item
                                thePlayer.Inventory.useItem(entry.Key.ToString());
                                success = true;
                                break;
                            }
                        }
                        if(success)
                        {
                            theExit.IsLocked = false;
                            return new CommandResponse("You unlock the door and put the key back in your pocket.");
                        }
                        else
                        {
                            return new CommandResponse("You are unable to unlock the door.");
                        }
                    }
                    else
                    {
                        return new CommandResponse("The door is not locked.");
                    }
                }
                else
                {
                    return response;
                }
            }
            return response;
        }


    }
}
