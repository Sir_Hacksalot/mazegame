﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;
using System.Collections;

namespace Mazegame.Control
{
    public class FleeCommand : Command
    {
        private static readonly Random rand = new Random();

        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            //get random exit
            Exit randomExit = thePlayer.CurrentLocation.GetExit(getRandom(thePlayer));
            thePlayer.CurrentLocation = randomExit.Destination;

            thePlayer.IsFighting = false;

            CommandResponse response = new CommandResponse("You bolt for the nearest exit...\n" 
                + thePlayer.CurrentLocation.ToString());
            return response;
        }
        private string getRandom(Player thePlayer)
        {
            Hashtable table = thePlayer.CurrentLocation.Exits;
            List<string> exitKeys = new List<string>();
   
            foreach (DictionaryEntry entry in table)
            {
                exitKeys.Add(entry.Key.ToString());
            }
            int size = exitKeys.Count;
            string randomKey = exitKeys[rand.Next(size)];

            return randomKey;

        }


    }
}
