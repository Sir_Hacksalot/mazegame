﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity;

namespace Mazegame.Control
{
    class SpeakCommand : Command
    {
        public override CommandResponse Execute(ParsedInput userInput, Player thePlayer)
        {
            if (userInput.Arguments.Count == 0)
            {
                return new CommandResponse("Who are you talking to?");
            }

            String npcName = (String)userInput.Arguments[0];

            NonPlayerCharacter desiredNPC = thePlayer.CurrentLocation.GetNPC(npcName);

            if (desiredNPC == null)
            {
                return new CommandResponse("There is nobody by that name");
            }
            
            if(desiredNPC.Hostile)
            {
                thePlayer.IsFighting = true;
                return new CommandResponse(desiredNPC.Speak() + "\n\nWill you FIGHT that idiot " + npcName + " or FLEE for your worthless life?");
            }
            else
            {
                return new CommandResponse(desiredNPC.Speak());
            }
           
            
        }
    }
}
