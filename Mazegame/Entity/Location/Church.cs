﻿using Mazegame.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Entity
{
    public class Church : Location
    {
        public Church(String description, String label) : base(description, label)
        {

        }

        public string pray(Player thePlayer)
        {
            StringBuilder prayer = new StringBuilder();
            prayer.Append("O Blessed Virgin,");
            prayer.Append("\n\nBlessed art thou,");
            prayer.Append("\nA monk swimming,");
            prayer.Append("\n......I forget the rest of it...");
            prayer.Append("\n\nAmen!");
            prayer.Append("\n\nYour strength increases by 1");
            thePlayer.Strength++;
            return prayer.ToString();
        }
    }
}
