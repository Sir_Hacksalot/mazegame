﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Entity
{
    public class Hotel : Location
    {
        public Hotel(String description, String label) : base(description, label)
        {

        }

        public string getDrink(Player thePlayer)
        {
            if(thePlayer.Inventory.Gold.Subtract(1))
            {
                thePlayer.Agility--;
                return "You pay 1gp and enjoy a refreshing drink. \nLose 1 agility point.";
            }
            else
            {
                return "You can't afford it.";
            }
            
        }
    }
}
