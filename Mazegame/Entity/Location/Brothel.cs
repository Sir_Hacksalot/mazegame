﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Entity
{
    public class Brothel : Hotel
    {
        public Brothel(string description, string label) : base(description, label)
        {

        }

        public string OohMatron(string act, Player thePlayer)
        {
            StringBuilder sb = new StringBuilder();
            if (thePlayer.Inventory.Gold.Subtract(5))
            {
                sb.Append("You decide to " + act + "!");
                sb.Append("\nOh yeah. Mmmmmmm. That feels good......");
                sb.Append("\nYou gain 1 life point.");
                return sb.ToString();
            }
            else
            {
                return "You can't afford it.";
            }    
        }
    }
}
