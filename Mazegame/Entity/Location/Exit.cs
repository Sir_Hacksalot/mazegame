
using System;

namespace Mazegame.Entity {
	public class Exit
	{
	    private String description;
	    private Location destination;
        private bool isLocked;

		public Exit(String description, Location destination, bool isLocked)
		{
		    this.description = description;
		    this.Destination = destination;
            this.isLocked = isLocked;
		}


	    public string Description
	    {
	        get { return description; }
	        set { description = value; }
	    }

	    public Location Destination
	    {
	        get { return destination; }
	        set { destination = value; }
	    }

        public bool IsLocked
        {
            get { return isLocked; }
            set { isLocked = value; }
        }
	}

}