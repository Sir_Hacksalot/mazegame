
using System;
using System.IO;

namespace Mazegame.Entity {
	public class Blacksmith : Shop
    {
        private static string[] path = new string[3] { "weapons.txt", "armour.txt", "shield.txt" };
        private const double buyBackPercentage = 0.2; 

        public Blacksmith(String description, String label) : base(description, label) 
        {
            init();      
        }

        public void init()
        {
            //fill the inventory
            for (int i = 0; i < 3; i++)
            {
                loadInventoryData(Inventory, path[i], i);
            }
        }

        public override string BuyItem(Player thePlayer, Item item, string argument)
        {
            //buy as per weapontable less 20%
            //item worth = worth - (worth * percentage/100);
            int tradePrice = Convert.ToInt32((double)item.Worth - ((double)item.Worth * buyBackPercentage));
            thePlayer.Inventory.Gold.Add(tradePrice);

            if(thePlayer.unequipItem(item))
            {
                Console.WriteLine("You unequip the item");
            }

            thePlayer.Inventory.removeItem(argument);          
            if(!thePlayer.CurrentLocation.Inventory.Items.ContainsKey(argument))
            {
                thePlayer.CurrentLocation.Inventory.Items.Add(argument, item);
            }
            return "You have sold the " + argument + " for " + tradePrice;
        }

        public override string SellItem(Player thePlayer, Item item, string argument)
        {
            if (thePlayer.Inventory.AddItem(item))
            {
                if (thePlayer.Inventory.Gold.Subtract(item.Worth))
                {
                    thePlayer.CurrentLocation.Inventory.Gold.Add(item.Worth);
                    thePlayer.CurrentLocation.Inventory.removeItem(argument);
                    return "You buy the " + argument;
                }
                else
                {
                    return "You can't afford the " + argument;
                }
            }
            else
            {
                return "You can't carry that much weight";
            }
        }

        public override Item getItemType(int type)
        {
            if(type == 0)
            {
                return new Weapon();
            }
            else if(type == 1)
            {
                return new Armor();
            }
            else if(type == 2)
            {
                return new Shield();
            }
            else
            {
                //this shouldn't ever happen....
                return new Weapon();
            }
        }

    }

}