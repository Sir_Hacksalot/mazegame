
using System;
using System.Collections;
using System.Text;

namespace Mazegame.Entity {
	public class Location
	{
	    private Hashtable exits;
        private Hashtable characters;
        private Inventory inventory;
        private string description;
        private string label;
 
	    public Location()
	    {
	    }

	    public Location(String description,String label)
		{
		    Description = description;
		    Label = label;
            exits = new Hashtable();
            characters = new Hashtable();
            inventory = new Inventory();
		}

	    public Boolean AddExit(String exitLabel, Exit theExit)
	    {
	        if (exits.ContainsKey(exitLabel))
            {
                return false;
            }

            exits.Add(exitLabel, theExit);
            return true;
	    }

        public Exit GetExit(String exitLabel)
        {
            return (Exit)exits[exitLabel];
        }

		public String Description
        {
			get{return description;}
			set{description = value;}
		}

        public Hashtable Exits
        {
            get { return exits; }
            set { exits = value; }
        }

        public String Label
	    {
	        get { return label; }
	        set { label = value; }
	    }

        public Inventory Inventory
        {
            get { return inventory; }
            set { inventory = value; }
        }

        public Hashtable Characters
        {
            get { return characters; }
            set { characters = value; }
        }

        public void AddNPC(string key, NonPlayerCharacter npc)
        {
            characters.Add(key, npc);
        }
        public void removeNPC(string key)
        {
            characters.Remove(key);
        }

        public String AvailableExits()
        {
            StringBuilder returnMsg = new StringBuilder();

            foreach(string label in this.exits.Keys)
            {
                returnMsg.Append(" " + label + " ");
            }
            return returnMsg.ToString();
        }
        public NonPlayerCharacter GetNPC(String npcName)
        {
            return (NonPlayerCharacter)characters[npcName];
        }

        public override string ToString()
        {
            return "**********\n" + this.label + "\n**********\n"
                + this.Description + "\n**********\n"
                + "Exits found :: " + AvailableExits() + "\n**********\n"
                + "Items found :: " + inventory.listItemsVertical() + "\n**********\n"
                + "People found :: " + availableNPC() + "\n**********\n";
        }

        public String availableNPC()
        {
            StringBuilder returnMsg = new StringBuilder();

            foreach (string label in this.characters.Keys)
            {
                returnMsg.Append(label + " ");
            }
            return returnMsg.ToString();
        }

        public bool ContainsExit(String exitLabel)
        {
            return exits.Contains(exitLabel);
        }

        public bool ContainsNPC(String npcLabel)
        {
            return characters.Contains(npcLabel);
        }

    }

}