

using System;
using System.IO;

namespace Mazegame.Entity {
	public abstract class Shop : Location
    {
        public Shop(String description, String label) : base(description, label)
        {

        }
        public Shop()
        {

		}     
        public abstract string SellItem(Player thePlayer, Item item, string argument);
        public abstract string BuyItem(Player thePlayer, Item item, string argument);
        public abstract Item getItemType(int type);
        public void loadInventoryData(Inventory inventory, String path, int id)
        {
            //
            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader reader = new StreamReader(path))
                    {
                        while (!reader.EndOfStream)
                        {
                            var readLine = reader.ReadLine();
                            if (readLine == null)
                            {
                                continue;
                            }
                            else
                            {
                                var lines = readLine.Split(' ');

                                Item temp = getItemType(id);

                                temp.Name = lines[0];
                                temp.Worth = Convert.ToInt32(lines[1]);
                                temp.Weight = Convert.ToInt32(lines[2]);
                                temp.Damage = lines[3];

                                inventory.Items.Add(temp.Name, temp);

                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Unable to find file");
                }

            }
            catch (Exception)
            {

                Console.WriteLine("Unable to find file");
            }
        }

    }

}