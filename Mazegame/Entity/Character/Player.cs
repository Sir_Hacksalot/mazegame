
using System;
using System.Text;

namespace Mazegame.Entity {
	public class Player : Character {

        private Location currentLocation;
        private bool isFighting = false;

        public Player() : base()
	    {

	    }

	    public Player(String name) : base()
		{
            Name = name;
            //beef up the player a little
            LifePoints += 10;
            Strength += 5;
        }

        public Location CurrentLocation
        {
            get { return currentLocation; }
            set { currentLocation = value; }
        }

        public bool IsFighting
        {
            get { return isFighting; }
            set { isFighting = value; }
        }

        public String ListItems()
        {
            StringBuilder str = new StringBuilder();
            str.Append(Inventory.listItemsVertical());
            str.Append(EquippedItemsList());
            str.Append(Inventory.Gold.ToString());
            return str.ToString();
        }

        public string getItem(string itemLabel, Item item)
        {
            if (Inventory.AddItem(item))
            {
                CurrentLocation.Inventory.removeItem(itemLabel);
                return "You take the " + itemLabel;
            }
            else
            {
                //I told him we've already got one...
                return "You can't carry that item.";
            }
        }
        public void dropItem(string itemLabel, Item item)
        {
            if (Inventory.removeItem(itemLabel))
            {
                //successfully removed, now drop it in the location
                CurrentLocation.Inventory.AddItem(itemLabel, item);
                if(unequipItem(item))
                {
                    Console.WriteLine("You have unequipped the item");
                }            
            }
            else
            {
                //I told him we've already got one...
                Console.WriteLine("You don't have one of those.");
            }
        }

        public bool unequipItem(Item item)
        {
            if (item.Equals(Weapon))
            {
                Weapon = new Weapon();
                return true;
            }
            else if (item.Equals(Armor))
            {
                Armor = new Armor();
                return true;
            }
            else if (item.Equals(Shield))
            {
                Shield = new Shield();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool equipItem(Object obj)
        {
            Type t = obj.GetType();

            if (t.Equals(typeof(Weapon)))
            {
                Weapon = (Weapon)obj;
                return true;
            }
            else if (t.Equals(typeof(Armor)))
            {
                Armor = (Armor)obj;
                return true;
            }
            else if (t.Equals(typeof(Shield)))
            {
                Shield = (Shield)obj;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

}