using Mazegame.Entity;

using System;
using System.Collections.Generic;
using System.Text;

namespace Mazegame.Entity
{
    public class Character
    {
        private int agility;
        private int lifePoints;
        private String name;
        private int strength; 
        public Dice dice;
        public Party party;
        private FiniteInventory inventory;
        private Weapon weapon;
        private Armor armor;
        private Shield shield;

        public Character()
        {
            agility = DiceRoller.getInstance().GenerateAbilityScore();
            strength = DiceRoller.getInstance().GenerateAbilityScore();
            lifePoints = DiceRoller.getInstance().GenerateAbilityScore();
            inventory = new FiniteInventory(strength);
            weapon = new Weapon();
            armor = new Armor();
            shield = new Shield();
        }

        public Character(String name) : base()
        {
            Name = name;
            
        }

        public int Agility
        {
            get { return agility; }
            set { agility = value; }
        }

        public int LifePoints
        {
            get { return lifePoints; }
            set { lifePoints = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Strength
        {
            get { return strength; }
            set { strength = value; }
        }

        public FiniteInventory Inventory
        {
            get { return inventory; }
            set { inventory = value; }
        }

        public Weapon Weapon
        {
            get { return weapon; }
            set { weapon = value; }
        }
        public Shield Shield
        {
            get { return shield; }
            set { shield = value; }
        }
        public Armor Armor
        {
            get { return armor; }
            set { armor = value; }
        }

        public string EquippedItemsList()
        {
            StringBuilder equipmentList = new StringBuilder();
            equipmentList.Append("\n|=========================================================|");
            equipmentList.Append("\n| Equipped Items                                          |");
            equipmentList.Append("\n|=========================================================|");

            equipmentList.Append("\n| Weapon :: ".PadRight(12) + weapon.Name
                                + "\n| Shield :: ".PadRight(12) + shield.Name
                                + "\n| Armor  :: ".PadRight(12) + armor.Name);
            return equipmentList.ToString();
        }
    } 
} 