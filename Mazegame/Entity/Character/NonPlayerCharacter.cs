
using System;
using System.Text;

namespace Mazegame.Entity
{
    public class NonPlayerCharacter : Character
    {
        private Boolean hostile;
        string phrase;

        public NonPlayerCharacter()
        {
        }

        public NonPlayerCharacter(String name, bool hostile, string phrase) : base()
        {
            this.hostile = hostile;
            Name = name;
            this.phrase = phrase;         
        }

        public Boolean Hostile
        {
            get { return hostile; }
            set { hostile = value; }
        }

        public string Speak()
        {
            StringBuilder talk = new StringBuilder();
            talk.Append(Name + " says: " + phrase);

            return talk.ToString();
        }

        public override string ToString()
        {
            return Name;
        }
    } 
} 