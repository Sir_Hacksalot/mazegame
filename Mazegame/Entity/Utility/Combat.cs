﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Entity
{
    class Combat
    {
        //welcome to fight club
        Location location;
        List<NonPlayerCharacter> npcList;
        List<NonPlayerCharacter> hostiles;
        List<Character> heroes;
        String initialHostile;
        Player thePlayer;

        private static readonly Random generator = new Random();

        public Combat(Player thePlayer, string initialHostile)
        {
            //fight!!!
            this.thePlayer = thePlayer;
            this.initialHostile = initialHostile;
            fightSetup();

        }

        private void fightSetup()
        {
            location = thePlayer.CurrentLocation;
            npcList = convertHashTableToList(location.Characters);
            hostiles = getHostiles(npcList);
            heroes = getHeroes(npcList);
            heroes.Insert(0, thePlayer);
        }

        private void cleanUp()
        {
            //put everybody back where they should be
            thePlayer.CurrentLocation.Characters.Clear();

            foreach (NonPlayerCharacter npc in hostiles)
            {
                thePlayer.CurrentLocation.Characters.Add(npc.Name, npc);
            }

            foreach (Character character in heroes)
            {
                if(character.Equals(thePlayer))
                {
                    thePlayer = (Player)character;
                }
                else
                {
                    thePlayer.CurrentLocation.Characters.Add(character.Name, character);
                }
                
            }
            
        }

        public bool fight()
        {
            //fight the first guy for a round
            attack(heroes[0], getInitialHostile());
            bringOutYourDead();

            //while you are alive AND there are still people to punch)
            while (heroes[0].LifePoints > 0 && hostiles.Count > 0)
            {
                //generate random combatants
                attack(heroes[getRandom(heroes.Count)], hostiles[getRandom(hostiles.Count)]);
                //sweep up the bodies
                bringOutYourDead();
            }

            cleanUp();

            if(thePlayer.LifePoints > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int getRandom(int count)
        {
            return generator.Next(count-1);
        }

        private void bringOutYourDead()
        {
            for (int i = hostiles.Count - 1; i > -1; i--)
            {
                if (hostiles[i].LifePoints <= 0)
                {
                    hostiles.RemoveAt(i);
                }
            }
            
            for (int i = heroes.Count - 1; i > 0; i--)
            {
                if (heroes[i].LifePoints <= 0)
                {
                    heroes.RemoveAt(i);
                }
            }
        }



        private void attack(Character hero, NonPlayerCharacter hostile)
        {
            //get armorclass
            int hostileArmorClass = getArmorClass(hostile);
            int heroArmorClass = getArmorClass(hero);

            //attack once each
            Attack(hostile, hero, hostileArmorClass);
            Attack(hero, hostile, heroArmorClass);
        }

        private static int getArmorClass(Character fighter)
        {
            return 10 + Convert.ToInt32(fighter.Armor.Damage) + Convert.ToInt32(fighter.Shield.Damage) + fighter.Agility;
        }

        private static void Attack(Character defender, Character attacker, int armorClass)
        {
            if (attacker.LifePoints > 0)
            {
                int offence = DiceRoller.getInstance().AttackRoll(1, 20, defender.Strength);
                Console.WriteLine(attacker.Name + " attacks " + defender.Name);
                if (offence >= armorClass)
                {
                    //the defender has been struck!!!!
                    int offenceDamageRoll = DiceRoller.getInstance().RollDamage(attacker.Weapon.Damage, attacker.Strength);
                    defender.LifePoints -= offenceDamageRoll;
                    Console.WriteLine(defender.Name + " gets smashed hard!!!");
                    if (defender.LifePoints <= 0)
                    {
                        Console.WriteLine(defender.Name + " has been killed.");
                    }
                }
                else
                {
                    Console.WriteLine(defender.Name + " deflects the attack!");
                }
            }
        }

        private static List<NonPlayerCharacter> getHostiles(List<NonPlayerCharacter> npcList)
        {
            List<NonPlayerCharacter> hostiles = new List<NonPlayerCharacter>();

            foreach (NonPlayerCharacter npc in npcList)
            {
                if (npc.Hostile)
                {
                    hostiles.Add(npc);
                }
            }
            return hostiles;
        }

        private static List<Character> getHeroes(List<NonPlayerCharacter> npcList)
        {
            List<Character> heroes = new List<Character>();

            foreach (NonPlayerCharacter npc in npcList)
            {
                if (!npc.Hostile)
                {
                    heroes.Add(npc);
                }
            }
            return heroes;
        }

        private static List<NonPlayerCharacter> convertHashTableToList(Hashtable htable)
        {
            List<NonPlayerCharacter> npcList = new List<NonPlayerCharacter>();

            foreach (DictionaryEntry entry in htable)
            {
                NonPlayerCharacter npc = (NonPlayerCharacter)entry.Value;
                npcList.Add(npc);
            }
            return npcList;
        }

        private NonPlayerCharacter getInitialHostile()
        {
            NonPlayerCharacter hostile = (NonPlayerCharacter)location.Characters[initialHostile];
            return hostile;
        }


    }
}
