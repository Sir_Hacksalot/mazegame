﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mazegame.Entity.Utility;

namespace Mazegame.Entity
{
    public class FiniteInventory : Inventory
    {
        private double weightLimit;

        public FiniteInventory(int strength) : base()
        {
           SetStrength(strength);
        }

        public void SetStrength(int strength)
        {
            weightLimit = (double)WeightLimit.GetInstance().GetModifier(strength);
        }

        public double GetWeight()
        {
            double currentWeight = 0;
            foreach (Item theItem in this.Items.Values)
            {
                currentWeight += theItem.Weight;
            }
            return currentWeight;
        }

        public virtual bool AddItem(Item theItem)
        {
            if (weightLimit > theItem.Weight + GetWeight())
                return base.AddItem(theItem.Name, theItem);
            return false;
        }
    }
}
