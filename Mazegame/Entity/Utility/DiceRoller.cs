﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Entity
{
    public class DiceRoller 
    {
        private static DiceRoller instance = new DiceRoller();

        private Dice die;

        private DiceRoller()
        {
            die = new Dice(6);
        }

        public static DiceRoller getInstance()
        {
            return instance;
        }

        //roll 5 six sided dice, take the best 3
        //return the total
        public int GenerateAbilityScore()
        {
            List<int> results = new List<int>();
            for (int i = 0; i < 5; i++)
            {
                results.Add(die.Roll());
            }

            results.Sort();
            results.RemoveRange(0, 2);
            return results.Sum();
        }

        public int AttackRoll(int dieQty, int sides, int strength)
        {
            die.Faces = sides;

            List<int> results = new List<int>();

            for (int i = 0; i < dieQty; i++)
            {
                results.Add(die.Roll());
            }

            results.Add(strength);
            return results.Sum();
        }

        public int RollDamage(string damage, int strength)
        {
            string[] value = damage.Split('d');
            return AttackRoll(Convert.ToInt32(value[0]), Convert.ToInt32(value[1]), strength);
        }
    }
}
