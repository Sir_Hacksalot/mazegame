﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Entity
{
    public class Inventory
    {
        private Money gold;
        private Hashtable items;

        public Inventory()
        {
            items = new Hashtable();
            gold = new Money();
        }

        public Money Gold
        {
            get { return gold; }
            set { gold = value; }
        }
        
        public Hashtable Items
        {
            get { return items; }
            set { items = value; }
        }

        //list items
        public string listItemsHorizontal()
        {
            string inventoryString = "";

            foreach (DictionaryEntry entry in Items)
            {
                inventoryString += "|= " + entry.Value.ToString() + " =|";
            }
            return inventoryString;
        }

        public string listItemsVertical()
        {
            StringBuilder str = new StringBuilder();
            str.Append("\n|=========================================================|");
            str.Append("\n| Inventory                                               |");
            str.Append("\n|=========================================================|");
            str.Append("\n| " + "Item".PadRight(20) + "Worth".PadLeft(10) + "Weight".PadLeft(10) + "   Damage/Bonus".PadRight(15) + " |");
            str.Append("\n|=========================================================|");

            foreach (DictionaryEntry entry in Items)
            {
                str.Append("\n| " + entry.Value.ToString());
            }

            return str.ToString();
        }

        public bool useItem(String itemLabel)
        {
            if (items.ContainsKey(itemLabel))
            {
                Item i = (Item)items[itemLabel];
                i.use();
                return true;  
            }
            Console.WriteLine("you don't have one of those!");
            return false;   
        }

        public bool AddItem(String itemLabel, Item item)
        {
            if (items.ContainsKey(itemLabel))
            {
                return false;
            }
            items.Add(itemLabel, item);
            return true;
        }

        public bool removeItem(String itemLabel)
        {
            if (items.ContainsKey(itemLabel))
            {
                items.Remove(itemLabel);
                return true;
            }
            return false;
        }
 
    }
}
