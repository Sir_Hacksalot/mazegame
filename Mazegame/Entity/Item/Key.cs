﻿using Mazegame.Boundary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mazegame.Entity
{
    public class Key : Item
    {
        public Key(string name, int worth, int weight, string description) : base(name, worth, weight, description)
        {
        }

        public override void use()
        {
            Console.WriteLine("You turn the key in the lock.");
        }

        
    }
}
