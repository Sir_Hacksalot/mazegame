
using System;

namespace Mazegame.Entity {
	public class Weapon : Item
    {
		public Dice m_Dice; //why do i need this???

        public Weapon() : base()
        {
            Damage = "0d0";
        }

        public Weapon(string name, int worth, int weight, string description) : base(name, worth, weight, description)
        {
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override void use()
        {
            Console.WriteLine("Swooosh!You attack with your weapon.");
        }
    }
}