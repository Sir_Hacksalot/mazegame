
using System;

namespace Mazegame.Entity
{
    public abstract class Item
    {
        private String name;
        private int worth;
        private int weight;
        private String damage;

        public Item()
        {
            name = "";
            worth = 0;
            weight = 0;
            damage = "0";
        }

        public Item(String name, int worth, int weight, string damage)
        {
            this.name = name;
            this.worth = worth;
            this.weight = weight;
            this.damage = damage;
        }

        public int Worth
        {
            get { return worth; }
            set { worth = value; }
        }

        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public string Damage
        {
            get { return damage; }
            set { damage = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public override string ToString()
        {
            if (this.name == null)
            {
                return "";
            }
            else
            {
                string output = name.PadRight(20) + (worth + "gp").PadLeft(10) 
                    + (weight + "lb").PadLeft(10) + "   " + damage.PadRight(13) + "|";
                return output;
            }
            
        }

        public abstract void use();

    } 
} 