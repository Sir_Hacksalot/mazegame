
using System;
using Mazegame.Boundary;
namespace Mazegame {
	public class SimpleConsoleClient : IMazeClient {

		public SimpleConsoleClient(){
            Console.Title = "Wenchville";
            Console.WindowWidth = 80;
            Console.ForegroundColor = ConsoleColor.Green;
		}

		public String GetReply(String question){
            Console.Out.Write("\n" + question + " ");
            return Console.In.ReadLine();
		}

		public void PlayerMessage(String message)
        {
            //Console.Beep();
            Console.Out.Write(message);
		}

        public String GetCommand()
        {
            Console.Out.Write("\n\n>>> ");
            return Console.In.ReadLine();
        }

	}

}