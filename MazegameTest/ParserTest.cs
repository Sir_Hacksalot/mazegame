﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using Mazegame.Control;

namespace MazegameTest
{
    /// <summary>
    /// Summary description for ParserTest
    /// </summary>
    [TestClass]
    public class ParserTest
    {
        private ArrayList commands;
        private Parser theParser;

        [TestInitialize]
        public void Init()
        {
            commands = new ArrayList(new string[] { "quit", "move" });
            theParser = new Parser(commands);
        }

        [TestMethod]
        public void TestCommandDetection()
        {
            ParsedInput userInput = theParser.Parse("quit");
            Assert.AreEqual(userInput.Command, "quit");
            userInput = theParser.Parse("move west");
            Assert.AreEqual(userInput.Command, "move");
            userInput = theParser.Parse("Tim");
            Assert.AreEqual(userInput.Command, "");
        }

        [TestMethod]
        public void TestArgumentDetection()
        {
            ParsedInput userInput = theParser.Parse("quit");
            Assert.IsTrue(userInput.Arguments.Count == 0);
            userInput = theParser.Parse("move west");
            Assert.IsTrue(userInput.Arguments.Count == 1);
            userInput = theParser.Parse("move to the west");
            Assert.IsTrue(userInput.Arguments.Count == 1);
        }
    }
}
