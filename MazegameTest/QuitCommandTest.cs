﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mazegame.Control;
using Mazegame.Entity;
using System.Collections;

namespace MazegameTest
{
    /// <summary>
    /// Summary description for QuitCommandTest
    /// </summary>
    [TestClass]
    public class QuitCommandTest
    {
        private ParsedInput playerInput;
        private Player thePlayer;
        private CommandHandler handler;
        private QuitCommand quit;

        [TestInitialize]
        public void Init()
        {
            playerInput = new ParsedInput("quit", new System.Collections.ArrayList());
            thePlayer = new Player("Tim");
            handler = new CommandHandler();
            quit = new QuitCommand();
        }

        

        [TestMethod]
        public void TestQuit()
        {
            //test quit command with no arguments
            CommandResponse response = quit.Execute(playerInput, thePlayer);
            Assert.IsTrue(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Goodbye"));

            //test quit comand >0 arguments
            playerInput.Arguments = new ArrayList(new string[] { "this", "game" });
            response = quit.Execute(playerInput, thePlayer);
            Assert.IsTrue(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Goodbye"));
        }

        [TestMethod]
        public void TestQuitHandler()
        {
            //test quit command with no arguments
            CommandResponse response = handler.ProcessTurn("quit", thePlayer);
            Assert.IsTrue(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Goodbye"));

            //test quit comand >0 arguments
            response = handler.ProcessTurn("quit this game", thePlayer);
            Assert.IsTrue(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Goodbye"));
        }
    }
}
