﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mazegame.Entity;

namespace MazegameTest
{
    [TestClass]
    public class LocationTest
    {
        private Location t127;
        private Location gregsOffice;

        [TestInitialize]
        public void init()
        {
            t127 = new Location("a lecture theatre", "T127");
            gregsOffice = new Location("a spinning vortex of terror", "Greg's Office");

        }

        [TestMethod]
        public void TextAddExit()
        {
            //add some exits, should return true
            Assert.IsTrue(t127.AddExit("south", new Exit("you see a mound of paper to the south", gregsOffice, false)));
            Assert.IsTrue(gregsOffice.AddExit("north", new Exit("you see a bleak place to the north", t127, false)));

            //should already have an exit so can't add another one
            Assert.IsFalse(t127.AddExit("south", new Exit("description", gregsOffice, false)));
        }

        [TestMethod]
        public void TestGetExit()
        {
            Exit myExit = new Exit("you see a mound of paper to the south", gregsOffice, false);
            t127.AddExit("south", myExit);
            Assert.AreSame(myExit, t127.GetExit("south"));
            Assert.IsNull(t127.GetExit("north"));
        }
    }
}
