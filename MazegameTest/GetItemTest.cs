﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mazegame.Entity;
using Mazegame.Entity.Utility;
using System.IO;
using Mazegame.Control;
using System.Collections;

namespace MazegameTest
{
    /// <summary>
    //  test load capacity is functioning
    //  test if you can put the item in your inventory
    //  test if you can put multiple items in your inventory
    //  test that the item leaves the location when it is taken
    //  test for weight restrictions
    /// </summary>
    [TestClass]
    public class GetItemTest
    {
        Player thePlayer;
        Location stables;
        Blacksmith smithy;
        Armor chainmail;
        Weapon sword;
        Shield buckler;
        Key rustyKey;
        Weapon veryHeavyHammerOfDeath;

        ParsedInput playerInput;
        CommandHandler handler;
        TakeCommand take;

        private static Dictionary<int, int> populateTable(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader reader = new StreamReader(path))
                    {
                        Dictionary<int, int> table = new Dictionary<int, int>();

                        while (!reader.EndOfStream)
                        {
                            var readLine = reader.ReadLine();
                            if (readLine == null)
                            {
                                continue;
                            }
                            else
                            {
                                var lines = readLine.Split(' ');
                                table.Add(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1]));
                            }
                        }
                        return table;
                    }
                }
                else
                {
                    Console.WriteLine("Unable to find file");
                    return new Dictionary<int, int>();
                }
            }
            catch (Exception)
            {
                return new Dictionary<int, int>();
            }
        }
         
        [TestInitialize]
        public void init()
        {
            WeightLimit.GetInstance(populateTable("weight.txt"));
            StrengthTable.GetInstance(populateTable("strength.txt"));
            AgilityTable.GetInstance(populateTable("agility.txt"));

            playerInput = new ParsedInput("take", new ArrayList());
            handler = new CommandHandler();
            take = new TakeCommand();

            smithy = new Blacksmith("Welcome to my forge! \nI buy and sell weapons, armour and shields.", "Durnik's Blacksmith Shop");
            stables = new Location("The Fiddlers hotel stables", "Horse Stables");
            chainmail = new Armor("chainmail", 150, 40, "5");
            sword = new Weapon("shortsword", 10, 3, "1d6");
            buckler = new Shield("buckler", 15, 5, "1");
            rustyKey = new Key("key", 1, 1, "");
            veryHeavyHammerOfDeath = new Weapon("hammerofdeath", 600, 500, "4d10");

            stables.Inventory.Items.Add("chainmail", chainmail);
            stables.Inventory.Items.Add("buckler", buckler);
            stables.Inventory.Items.Add("key", rustyKey);
            stables.Inventory.Items.Add("shortsword", sword);
            stables.Inventory.Items.Add("hammerofdeath", veryHeavyHammerOfDeath);

            thePlayer = new Player("Tim");
            thePlayer.CurrentLocation = stables;

        }

        [TestMethod]
        public void TestTakeNothing()
        {
            Assert.AreSame(stables, thePlayer.CurrentLocation);
            //test sell command no arguments
            CommandResponse response = take.Execute(playerInput, thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("I don't know what you want"));
        }

        [TestMethod]
        public void TestTakeNoMatch()
        {
            playerInput.Arguments = new ArrayList(new String[] { "elephant" });
            CommandResponse response = take.Execute(playerInput, thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("You can't have that object."));
        }

        [TestMethod]
        public void TestTakeMatch()
        {
            thePlayer.Strength = 15;
            playerInput.Arguments = new ArrayList(new String[] { "shortsword" });
            CommandResponse response = take.Execute(playerInput, thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("You take the"));
        }

        [TestMethod]
        public void TestTakeBlacksmith()
        {
            thePlayer.CurrentLocation = smithy;
            CommandResponse response = handler.ProcessTurn("take shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Not a valid command"));
        }


        [TestMethod]
        public void TestLoadCapacity()
        {
            thePlayer.Strength = 15;
            int loadCapacity = WeightLimit.GetInstance().GetModifier(thePlayer.Strength);
            Assert.AreEqual(loadCapacity, 133);
            thePlayer.Strength = 7;
            loadCapacity = WeightLimit.GetInstance().GetModifier(thePlayer.Strength);
            Assert.AreEqual(loadCapacity, 46);
            thePlayer.Strength = 30; //off the charts
            loadCapacity = WeightLimit.GetInstance().GetModifier(thePlayer.Strength);
            Assert.AreEqual(loadCapacity, 933);
        }

        [TestMethod]
        public void TestPutItemInInventory()
        {
            //make sure our hero can carry this stuff, bump him up to 266lb
            thePlayer.Strength = 20;

            thePlayer.getItem("key", rustyKey);
            Assert.IsTrue(thePlayer.Inventory.Items.ContainsKey("key"));
            thePlayer.getItem("buckler", buckler);
            Assert.IsTrue(thePlayer.Inventory.Items.ContainsKey("buckler"));
            thePlayer.getItem("shortsword", sword);
            Assert.IsTrue(thePlayer.Inventory.Items.ContainsKey("shortsword"));
            thePlayer.getItem("chainmail", chainmail);
            Assert.IsTrue(thePlayer.Inventory.Items.ContainsKey("chainmail"));
        }
        [TestMethod]
        public void TestItemIsGoneFromLocation()
        {
            //make sure our hero can carry this stuff, bump him up to 266lb
            thePlayer.Strength = 20;

            thePlayer.getItem("key", rustyKey);
            Assert.IsFalse(thePlayer.CurrentLocation.Inventory.Items.ContainsKey("key"));
            thePlayer.getItem("buckler", buckler);
            Assert.IsFalse(thePlayer.CurrentLocation.Inventory.Items.ContainsKey("buckler"));
            thePlayer.getItem("shortsword", sword);
            Assert.IsFalse(thePlayer.CurrentLocation.Inventory.Items.ContainsKey("shortsword"));
            thePlayer.getItem("chainmail", chainmail);
            Assert.IsFalse(thePlayer.CurrentLocation.Inventory.Items.ContainsKey("chainmail"));
        }

        [TestMethod]
        public void TestPlayerCarryingCapacity()
        {
            //make sure our hero can carry this stuff, bump him to 46lb
            thePlayer.Strength = 7;
            Assert.AreEqual(chainmail.Weight + rustyKey.Weight, 41);
            Assert.AreEqual(thePlayer.getItem("key", rustyKey), "You take the key");
            Assert.AreEqual(thePlayer.getItem("chainmail", chainmail), "You take the chainmail");
            //this one should break him
            Assert.AreEqual(thePlayer.getItem("hammerofdeath", veryHeavyHammerOfDeath), "You can't carry that item.");
        }
    }
}
