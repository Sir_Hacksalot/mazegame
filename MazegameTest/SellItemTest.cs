﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mazegame.Entity;
using System.IO;
using Mazegame.Entity.Utility;
using Mazegame.Control;
using System.Collections;

namespace MazegameTest
{
    /// <summary>
    /// Summary description for SellItemTest
    /// </summary>
    [TestClass]
    public class SellItemTest
    {
        Player thePlayer;

        ParsedInput playerInput;
        CommandHandler handler;
        SellCommand sell;

        Location stables;
        Blacksmith smithy;
        Brothel brothel;
        Hotel fiddlerHotel;
        Church stBrigids;

        Armor chainmail;
        Weapon sword;
        Shield buckler;
        Key rustyKey;
        Weapon veryHeavyHammerOfDeath;

        private static Dictionary<int, int> populateTable(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader reader = new StreamReader(path))
                    {
                        Dictionary<int, int> table = new Dictionary<int, int>();

                        while (!reader.EndOfStream)
                        {
                            var readLine = reader.ReadLine();
                            if (readLine == null)
                            {
                                continue;
                            }
                            else
                            {
                                var lines = readLine.Split(' ');
                                table.Add(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1]));
                            }
                        }
                        return table;
                    }
                }
                else
                {
                    Console.WriteLine("Unable to find file");
                    return new Dictionary<int, int>();
                }
            }
            catch (Exception)
            {
                return new Dictionary<int, int>();
            }
        }

        [TestInitialize]
        public void init()
        {
            WeightLimit.GetInstance(populateTable("weight.txt"));
            StrengthTable.GetInstance(populateTable("strength.txt"));
            AgilityTable.GetInstance(populateTable("agility.txt"));

            playerInput = new ParsedInput("sell", new ArrayList());
            handler = new CommandHandler();
            sell = new SellCommand();

            stables = new Location("The Fiddlers hotel stables", "Horse Stables");
            smithy = new Blacksmith("Welcome to my forge! \nI buy and sell weapons, armour and shields.", "Durnik's Blacksmith Shop");
            fiddlerHotel = new Hotel("The worst drinking establishment in town", "The Fiddler's Phallus");
            stBrigids = new Church("A small stone chapel", "Chapel of St. Brigid");
            brothel = new Brothel("Welcome hero! \nWe hope you enjoy our naughty wenches!", "Hosies House of Whores");
            chainmail = new Armor("chainmail", 150, 40, "5");
            sword = new Weapon("shortsword", 10, 3, "1d6");
            buckler = new Shield("buckler", 15, 5, "1");
            rustyKey = new Key("key", 1, 1, "");
            veryHeavyHammerOfDeath = new Weapon("hammerofdeath", 600, 500, "4d10");

            thePlayer = new Player("Tim");
            thePlayer.Strength = 30; //totally ripped, dude
            thePlayer.CurrentLocation = smithy;

            thePlayer.Inventory.AddItem("chainmail", chainmail);
            thePlayer.Inventory.AddItem("buckler", buckler);
            thePlayer.Inventory.AddItem("key", rustyKey);
            thePlayer.Inventory.AddItem("shortsword", sword);
            thePlayer.Inventory.AddItem("hammerofdeath", veryHeavyHammerOfDeath);
        }

        [TestMethod]
        public void TestSellNothing()
        {
            Assert.AreSame(smithy, thePlayer.CurrentLocation);
            //test sell command no arguments
            CommandResponse response = sell.Execute(playerInput, thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("I don't know what you want"));
        }

        [TestMethod]
        public void TestSellNoMatch()
        {
            playerInput.Arguments = new ArrayList(new String[] { "elephant" });
            CommandResponse response = sell.Execute(playerInput, thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("That isn't going to work."));
        }

        [TestMethod]
        public void TestSellMatch()
        {
            playerInput.Arguments = new ArrayList(new String[] { "shortsword" });
            CommandResponse response = sell.Execute(playerInput, thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("You have sold the"));
        }

        //"You have sold the " + argument + " for " + tradePrice;

        [TestMethod]
        public void TestSellStables()
        {
            thePlayer.CurrentLocation = stables;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Not a valid command"));
        }

        [TestMethod]
        public void TestSellHotel()
        {
            thePlayer.CurrentLocation = fiddlerHotel;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Not a valid command"));
        }

        [TestMethod]
        public void TestSellBrothel()
        {
            thePlayer.CurrentLocation = brothel;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Not a valid command"));
        }

        [TestMethod]
        public void TestSellChurch()
        {
            thePlayer.CurrentLocation = stBrigids;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("Not a valid command"));
        }

        [TestMethod]
        public void TestSellBlacksmith()
        {
            thePlayer.CurrentLocation = smithy;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Contains("You have sold the"));
        }

        [TestMethod]
        public void TestSellPrice()
        {
            int sellingPrice = Convert.ToInt32((double)sword.Worth - ((double)sword.Worth * 0.2));
            thePlayer.CurrentLocation = smithy;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            Assert.IsTrue(response.Message.Equals("You have sold the " + "shortsword" + " for " + sellingPrice));
        }

        [TestMethod]
        public void TestPlayerGotPaid()
        {
            Money money = thePlayer.Inventory.Gold;
            int sellingPrice = Convert.ToInt32((double)sword.Worth - ((double)sword.Worth * 0.2));
            thePlayer.CurrentLocation = smithy;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(response.FinishedGame);
            money.Add(sellingPrice);
            Assert.AreEqual(money, thePlayer.Inventory.Gold);
        }

        [TestMethod]
        public void TestRemovedFromInventory()
        {
            Assert.IsTrue(thePlayer.Inventory.Items.Contains("shortsword"));
            thePlayer.CurrentLocation = smithy;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsFalse(thePlayer.Inventory.Items.Contains("shortsword"));
        }

        [TestMethod]
        public void TestItemAddedToShopInventory()
        {
            thePlayer.CurrentLocation = smithy;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsTrue(thePlayer.CurrentLocation.Inventory.Items.Contains("shortsword"));
        }

        [TestMethod]
        public void TestItemTypesThatCanBeSold()
        {
            thePlayer.CurrentLocation = smithy;
            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);
            Assert.IsTrue(response.Message.Contains("You have sold the"));
            response = handler.ProcessTurn("sell buckler", thePlayer);
            Assert.IsTrue(response.Message.Contains("You have sold the"));
            response = handler.ProcessTurn("sell chainmail", thePlayer);
            Assert.IsTrue(response.Message.Contains("You have sold the"));
            response = handler.ProcessTurn("sell key", thePlayer);
            Assert.IsTrue(response.Message.Contains("You have sold the"));
        }

        [TestMethod]
        public void TestSoldItemIsUnEquipped()
        {
            thePlayer.CurrentLocation = smithy;

            thePlayer.equipItem(sword);
            Assert.AreEqual(thePlayer.Weapon, sword);
            thePlayer.equipItem(chainmail);
            Assert.AreEqual(thePlayer.Armor, chainmail);
            thePlayer.equipItem(buckler);
            Assert.AreEqual(thePlayer.Shield, buckler);

            CommandResponse response = handler.ProcessTurn("sell shortsword", thePlayer);

            Assert.AreNotEqual(thePlayer.Weapon, sword);
            response = handler.ProcessTurn("sell buckler", thePlayer);
            Assert.AreNotEqual(thePlayer.Shield, buckler);       
            response = handler.ProcessTurn("sell chainmail", thePlayer);
            Assert.AreNotEqual(thePlayer.Armor, chainmail);
        }

    }
}
