

using System;
using Mazegame.Boundary;
using Mazegame.Entity;
using System.IO;
using System.Collections.Generic;
using Mazegame.Entity.Utility;

namespace MazeDataImp1
{
    public class HardCodedData : IMazeData
    {
        private Location cemetery;
        private Location villageGreen;
        private Brothel brothel;
        private Location townSquare;
        private Blacksmith smithy;
        private Hotel fiddlerHotel;
        private Church stBrigids;
        private Hotel bishopHotel;
        private Location stables;

        Weapon dagger;
        Weapon rapier;
        Weapon battleaxe;
        Weapon sword;
        Weapon scimitar;
        Weapon longspear;
        Weapon nunchaku;

        Armor chainmail;
        Armor scalemail;
        Armor leather;

        Shield buckler;

        public HardCodedData()
        {
            getWeightTable();
            getStrengthTable();
            getAgilityTable();
            createLocations();
            createItems();
            createNPC();          
        }

        private void getAgilityTable()
        {
            AgilityTable.GetInstance(populateTable("agility.txt"));
        }

        private static void getStrengthTable()
        {
            StrengthTable.GetInstance(populateTable("strength.txt"));
        }

        private void getWeightTable()
        {
            WeightLimit.GetInstance(populateTable("weight.txt"));
        }

        private static Dictionary<int, int> populateTable(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader reader = new StreamReader(path))
                    {
                        Dictionary<int, int> table = new Dictionary<int, int>();

                        while (!reader.EndOfStream)
                        {
                            var readLine = reader.ReadLine();
                            if (readLine == null)
                            {
                                continue;
                            }
                            else
                            {
                                var lines = readLine.Split(' ');
                                table.Add(Convert.ToInt32(lines[0]), Convert.ToInt32(lines[1]));
                            }
                        }
                        return table;
                    }
                }
                else
                {
                    Console.WriteLine("Unable to find file");
                    return new Dictionary<int, int>();
                }
            }
            catch (Exception)
            {
                return new Dictionary<int, int>();
            }
        }

        
        ~HardCodedData()
        {
        }

        public virtual void Dispose()
        {
        }
 

        public Location GetStartingLocation()
        {
            return cemetery;
        }

        public String GetWelcomeMessage()
        {
            return loadTitleGraphic() + "->Welcome to Wenchville.\n\n";
        }

        private String loadTitleGraphic()
        {
            string path = "title.txt";
            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader reader = new StreamReader(path))
                    {
                        String text = "";

                        while (!reader.EndOfStream)
                        {
                            text = reader.ReadToEnd();
                        }
                        return text;
                    }
                }
                else
                {
                    Console.WriteLine("Unable to find file");
                }
            }
            catch (Exception)
            {

                Console.WriteLine("Unable to find file");
            }
            return "";
        }

        private void createLocations()
        { 
            cemetery = new Location("A dark, dank, possibly haunted graveyard on the \nedge of a shitty looking town", "Graveyard");
            villageGreen = new Location("An open space of green grass and butterflies", "Village Green");
            brothel = new Brothel("Welcome hero! \nWe hope you enjoy our naughty wenches!", "Hosies House of Whores");
            townSquare = new Location("An open area in the centre of town", "Town Square");
            smithy = new Blacksmith("Welcome to my forge! \nI buy and sell weapons, armour and shields.", "Durnik's Blacksmith Shop");
            fiddlerHotel = new Hotel("The worst drinking establishment in town", "The Fiddler's Phallus");
            stBrigids = new Church("A small stone chapel", "Chapel of St. Brigid");
            bishopHotel = new Hotel("A grand looking hotel", "The Bishop's Elbow");
            stables = new Location("The Fiddlers hotel stables", "Horse Stables");

            cemetery.AddExit("north", new Exit("you see an open space to the north", villageGreen, false));
            cemetery.AddExit("northeast", new Exit("you see a house to the northeast. All the curtains are drawn", brothel, false));
            
            villageGreen.AddExit("north", new Exit("you see a busy blacksmith shop to the north", smithy, false));
            villageGreen.AddExit("south", new Exit("you see a spooky graveyard to the south", cemetery, false));
            villageGreen.AddExit("northeast", new Exit("you see an open courtyard to the northeast", townSquare, false));
            villageGreen.AddExit("east", new Exit("you see a house to the east. \nYou can hear a lot of squealing coming from within", brothel, false));
            
            brothel.AddExit("northwest", new Exit("you see an open courtyard to the northeast", townSquare, false));
            brothel.AddExit("west", new Exit("you see an open grassy space to the west", villageGreen, false));
            brothel.AddExit("southwest", new Exit("you see a spooky graveyard to the southwest", cemetery, false));
            brothel.AddExit("east", new Exit("you see a fine looking hotel to the east", bishopHotel, false));
            

            bishopHotel.AddExit("north", new Exit("you see a small stone chapel to the north", stBrigids, true));
            bishopHotel.AddExit("west", new Exit("you see several ladies waving and blowing kisses in your direction", brothel, false));
            
            stBrigids.AddExit("west", new Exit("you see an open courtyard to the west", townSquare, false));
            stBrigids.AddExit("south", new Exit("you see a fine looking hotel to the east", bishopHotel, true));
            
            townSquare.AddExit("west", new Exit("you see a busy blacksmith shop to the west", smithy, false));
            townSquare.AddExit("southwest", new Exit("you see an open area full of green grass. \nThere is an ancient oak in the centre", villageGreen, false));
            townSquare.AddExit("southeast", new Exit("you see a house to the southeast", brothel, false));
            townSquare.AddExit("east", new Exit("you see a small stone chapel to the east", stBrigids, true));
            
            smithy.AddExit("west", new Exit("you notice a seedy looking pub to the west", fiddlerHotel, false));
            smithy.AddExit("south", new Exit("you see an open area full of green grass. \nRabbits and goats frolic among the dandelions", villageGreen, false));
            smithy.AddExit("northwest", new Exit("you can just make out what appears to be a stable for horses", stables, false));
            smithy.AddExit("east", new Exit("an open square is to the east", townSquare, false));

            fiddlerHotel.AddExit("north", new Exit("the hotel stables are to the north", stables, false));
            fiddlerHotel.AddExit("east", new Exit("a bustling blacksmith forge", smithy, false));
            
            stables.AddExit("south", new Exit("a really awful pub", fiddlerHotel, false));
            stables.AddExit("southeast", new Exit("a busy looking forge", smithy, false));          
        }

        private void createItems()
        {
            Key rustyKey = new Key("key", 1, 1, "");

            dagger = new Weapon("dagger", 1, 2, "1d4");
            rapier = new Weapon("rapier", 20, 3, "1d6");
            battleaxe = new Weapon("battleaxe", 10, 7, "1d8");
            sword = new Weapon("shortsword", 10, 3, "1d6");
            scimitar = new Weapon("scimitar", 15, 4, "1d6");
            longspear = new Weapon("longspear", 5, 9, "1d8");
            nunchaku = new Weapon("nunchaku", 2, 2, "1d6");

            chainmail = new Armor("chainmail", 150, 40, "5");
            scalemail = new Armor("scalemail", 50, 30, "4");
            leather = new Armor("leather", 10, 15, "2");

            buckler = new Shield("buckler", 15, 5, "1");

            cemetery.Inventory.Items.Add("dagger", dagger);
            villageGreen.Inventory.Items.Add("scalemail", scalemail);
            villageGreen.Inventory.Items.Add("battleaxe", battleaxe);
            brothel.Inventory.Items.Add("key", rustyKey);
            brothel.Inventory.Items.Add("buckler", buckler);
            bishopHotel.Inventory.Items.Add("shortsword", sword);
            stBrigids.Inventory.Items.Add("scimitar", scimitar);
            townSquare.Inventory.Items.Add("buckler", buckler);
            fiddlerHotel.Inventory.Items.Add("chainmail", chainmail);
            fiddlerHotel.Inventory.Items.Add("rapier", rapier);
            stables.Inventory.Items.Add("buckler", buckler);
            stables.Inventory.Items.Add("longspear", longspear);
        }

        private void createNPC()
        {
            NonPlayerCharacter durnik = new NonPlayerCharacter("Durnik", false, "I'm a little bit busy right now");
            NonPlayerCharacter molly = new NonPlayerCharacter("Molly", false, "Hey sweetheart!");
            NonPlayerCharacter susie = new NonPlayerCharacter("Susie", false, "You look like lots of fun! Hee hee hee!");
            NonPlayerCharacter hosie = new NonPlayerCharacter("Hosie", false, "Relax...my girls will take good care of you");
            NonPlayerCharacter sally = new NonPlayerCharacter("Sally", false, "I'm a bit kinky!");
            NonPlayerCharacter xena = new NonPlayerCharacter("Xena", false, "I have many skills...");
            NonPlayerCharacter charlotte = new NonPlayerCharacter("Charlotte", true, "Don't touch me!");
            NonPlayerCharacter leonard = new NonPlayerCharacter("Leonard", false, "I like chips");
            NonPlayerCharacter turnip = new NonPlayerCharacter("Turnip", false, "Don't talk to me!");
            NonPlayerCharacter priest = new NonPlayerCharacter("Pell", false, "I'm not here to defend the indefensible");
            NonPlayerCharacter simon = new NonPlayerCharacter("Simon", true, "I'm gonna smash your face in!");
            NonPlayerCharacter terry = new NonPlayerCharacter("Terry", true, "What are you bloody looking at?");
            NonPlayerCharacter shane = new NonPlayerCharacter("Shane", true, "Me and the boys are gonna kick the crap out of you!");
            NonPlayerCharacter dirk = new NonPlayerCharacter("Dirk", true, "Speak english will ya?");
            NonPlayerCharacter barmaid = new NonPlayerCharacter("barmaid", false, "You look like you need another drink");
            NonPlayerCharacter sheila = new NonPlayerCharacter("Sheila", false, "Where are you off to later?");
            NonPlayerCharacter landlord = new NonPlayerCharacter("Landlord", true, "You're barred from here mate. Piss off!");
            NonPlayerCharacter stableboy = new NonPlayerCharacter("stableboy", false, "Please don't tell anyone what I was doing!");

            charlotte.Weapon = dagger;
            hosie.Weapon = dagger;
            molly.Weapon = rapier;
            dirk.Weapon = dagger;
            barmaid.Weapon = dagger;
            xena.Weapon = sword;
            xena.Armor = leather;
            terry.Armor = chainmail;
            terry.Weapon = sword;
            terry.Shield = buckler;
            landlord.Weapon = scimitar;
            simon.Weapon = nunchaku;

            smithy.AddNPC("durnik", durnik);
            brothel.AddNPC("molly", molly);
            brothel.AddNPC("sally", sally);
            brothel.AddNPC("xena", xena);
            brothel.AddNPC("charlotte", charlotte);
            brothel.AddNPC("susie", susie);
            brothel.AddNPC("hosie", hosie);
            townSquare.AddNPC("leonard", leonard);
            bishopHotel.AddNPC("turnip", turnip);
            bishopHotel.AddNPC("pell", priest);
            bishopHotel.AddNPC("barmaid", barmaid);
            fiddlerHotel.AddNPC("shane", shane);
            fiddlerHotel.AddNPC("dirk", dirk);
            fiddlerHotel.AddNPC("sheila", sheila);
            fiddlerHotel.AddNPC("simon", simon);
            fiddlerHotel.AddNPC("terry", terry);
            fiddlerHotel.AddNPC("barmaid", barmaid);
            fiddlerHotel.AddNPC("landlord", landlord);
            stables.AddNPC("stableboy", stableboy);
        }
    } 
} 